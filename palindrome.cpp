#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

// Funksioni për të kontrolluar nëse një string është palindrome
bool isPalindrome(string str) {
    string reversed_str = str;
    reverse(reversed_str.begin(), reversed_str.end());
    return str == reversed_str;
}

int main() {
    string fjala;
    
    cout << "Vendosni fjalën: ";
    cin >> fjala;

    if (isPalindrome(fjala)) {
        cout << fjala << " është palindrome." << endl;
    } else {
        cout << fjala << " nuk është palindrome." << endl;
    }

    return 0;
}
